package org.deroesch.jsonxml;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CarTest {

	Car c = null;

	@BeforeEach
	void setUp() throws Exception {
		c = new Car();
	}

	@Test
	final void testGetDate() {
		assert (c.getDate().toString().length() > 0);
	}

	@Test
	final void testGetMake() {
		assertEquals("Ford", c.getMake());
	}

	@Test
	final void testGetModel() {
		assertEquals("Falcon", c.getModel());
	}

}
