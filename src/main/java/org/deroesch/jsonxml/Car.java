package org.deroesch.jsonxml;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Car {

	private Date date = new Date();
	private String make = "Ford";
	private String model = "Falcon";

	public Car() {
		super();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Car [date=" + date + ", make=" + make + ", model=" + model + "]";
	}

}
